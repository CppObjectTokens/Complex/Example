#! /usr/bin/env bash
# -*- coding: utf-8 -*-

Formatter=ProjectFormatter/FormatProjectSources.py
ComplexRoot=../../..

$Formatter $ComplexRoot --check
