# CppOtl examples

Examples for the [C++ Object Token Library](https://gitlab.com/CppObjectTokens/Module/Library/CppOtl).

* [TraitPrinter](https://gitlab.com/CppObjectTokens/Module/Example/TraitPrinter)
* [TransferUnique](https://gitlab.com/CppObjectTokens/Module/Example/TransferUnique)
* [WeakUnique](https://gitlab.com/CppObjectTokens/Module/Example/WeakUnique)
* [Car](https://gitlab.com/CppObjectTokens/Module/Example/Car)

---

Clone all examples:

```
git clone --recurse-submodules https://gitlab.com/CppObjectTokens/Complex/Example.git
cd Example
git submodule update --remote --recursive
```

Use CMake to build examples.

Note: Try the "feature/Cpp20Concepts" branch if a compiler supports C++20 Concepts.
